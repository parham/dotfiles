#include <unistd.h>
#include <fcntl.h>

/*
	written by Parham Barzegar, Sep 2018.
*/

#define BL15N "/home/parham/.cache/bl15n"
#define UP90N "/home/parham/.cache/up90n"
int strcmp(const char *s1, const char *s2);
char *stpcpy(char *dest, const char *src);
size_t strlen(const char *s);
char *strchrnul(const char *s, int c);
void *malloc(size_t size);
int atoi(const char *nptr);
double pow(double x, double y);

unsigned short int idigits(unsigned short int n) {
  unsigned short int i = 1;
  while(n /= 10)
		i++;
  return i;
}

char* itoa(int n) {
  char* st = malloc(idigits(n)+1);
  unsigned short int i;
	const unsigned short len = idigits(n);
  i = len;
  do {
    *(st+len-i) = n/(int)pow(10, i-1) % 10 + 48;
    i--;
  } while (i);
	if(len == 1) {
		*(st+1) = *st;
		*st = '0';
	}
  return st;
}

char* readfile(char* path, unsigned int n) {
	short int fd = 0;
	char* rvalue = malloc(n+1);
	read((fd = open(path, O_RDONLY)), rvalue, n);
	*(strchrnul(rvalue, '\n')) = '\0';
	close(fd);
	return rvalue;
}

int main() {
	double loading = 0.0;
	int charging = (!strcmp(readfile("/sys/class/power_supply/BAT0/status", 12), "Charging")), full = atoi(readfile("/sys/class/power_supply/BAT0/energy_full", 9));
	loading = charging ? (double)((full)-atoi(readfile("/sys/class/power_supply/BAT0/energy_now", 9)))/atoi(readfile("/sys/class/power_supply/BAT0/power_now", 9)) : (double)atoi(readfile("/sys/class/power_supply/BAT0/energy_now", 9))/atoi(readfile("/sys/class/power_supply/BAT0/power_now", 9));
	int minutes = (int)((loading - (int)loading) * 60.0);
 	char* capacity = readfile("/sys/class/power_supply/BAT0/capacity", 3);
	if(strlen(capacity) < 2) {
		*(capacity+1) = *capacity;
		*capacity = '0';
	}
	int i = 2, cap = atoi(capacity);
	while(i--) {
		char* ret = malloc(20);
		stpcpy(stpcpy(stpcpy(stpcpy(stpcpy(stpcpy(stpcpy(stpcpy(ret, capacity), (charging ? "%+ " : "%- ")), itoa((int)(loading))), ":"), itoa(minutes)), " "), itoa((double)(full)/atoi(readfile("/sys/class/power_supply/BAT0/energy_full_design", 9))*100.0)), "%\n");
		write(1, ret, 20);
	}

	mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP;;
	int res = 0;
	char* bl15 = readfile(BL15N, 1);
	if(cap >= 90 || cap <= 15) {
		res = 33;
		if(strcmp(bl15, "1") != 0 && !charging && cap <= 15) {
    	int fd;
    	write((fd = open(BL15N, O_RDWR | O_CREAT | O_TRUNC, mode)), "1", 1);
    	close(fd);
			execl("/home/parham/.scripts/bin/notification", "Battery!", "Low Battery", "Battery capacity is low. Please plug the adapter", (char *)NULL);
		} else if(strcmp(readfile(UP90N, 1), "1") != 0 && charging && cap >= 90) {
			int fd;
      write((fd = open(UP90N, O_RDWR | O_CREAT | O_TRUNC, mode)), "1", 1);
      close(fd);
			execl("/home/parham/.scripts/bin/notification", "Battery!", "High Battery", "Battery is charged. Please unplug the adapter.", (char *)NULL);
		}
	} else {
		if(strcmp(bl15, "1") == 0) {
			int fd;
			write((fd = open(BL15N, O_RDWR | O_TRUNC, mode)), "0", 1);
			close(fd);
		} else if(!strcmp(readfile(UP90N, 1), "1")) {
			int fd;
			write((fd = open(UP90N, O_RDWR | O_TRUNC, mode)), "0", 1);
			close(fd);
		}
	}
	return res;
}
