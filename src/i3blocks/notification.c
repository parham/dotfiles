#include <libnotify/notify.h>
#include <fcntl.h>

int open(const char *path, int oflag, ...);
/*
	written by Parham Barzegar, Sep 2018.
*/

int main(int argc, char* argv[]) {
  notify_init(argv[0]);
  NotifyNotification * LowBat = notify_notification_new(argv[1], argv[2], "dialog-information");
  notify_notification_show (LowBat, NULL);
  g_object_unref(G_OBJECT(LowBat));
  notify_uninit();
	if(fork() == 0) {
		dup2(open("/dev/null", O_WRONLY), 1);
		dup2(open("/dev/null", O_WRONLY), 2);
		execlp("/usr/bin/play", "play", "/home/parham/Music/not/slow-spring-board.mp3", (char*)NULL);
	}
  return 0;
}
