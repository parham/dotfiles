#include <unistd.h>
#include <fcntl.h>
/*
	written by Parham Barzegar, Sep 2018.
*/

int main() {
  char buf[4];
  int loadavg;
  if((loadavg = open("/proc/loadavg", O_RDONLY)) > 0) {
    read(loadavg, buf, 4);
    i = 2;
    while(i -= 1) {
      write(1, buf, 4);
      write(1, "\n", 1);
    }
    return 0;
  }
  return 1;
}
